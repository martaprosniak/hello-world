package pl.codementors.helloworld;

import java.util.Scanner;

/**
 * Welcome class.
 * @author psysiu
 */
public class HelloWorld {

    /**
     * Welcome method.
     * @param args Application starting parameters.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
        
        byte byteVar = 1;
        short shortVar = 1;
        int intVar = 1;
        long longVar = 1;
        float floatVar = 1;
        double doubleVar = 1;
        boolean booleanVar = true;
        char charVar = 'a';
        
        System.out.println(byteVar);
        System.out.println(shortVar);
        System.out.println(intVar);
        System.out.println(longVar);
        System.out.println(floatVar);
        System.out.println(doubleVar);
        System.out.println(booleanVar);
        System.out.println(charVar);
        
        Scanner inputScanner = new Scanner(System.in);
        System.out.println("Please enter int");
	intVar = inputScanner.nextInt();
        System.out.println(intVar);

	System.out.println("Please enter long");
	longVar = inputScanner.nextLong();
        System.out.println(longVar);

	System.out.println("Please enter byte");
	byteVar = inputScanner.nextByte();
        System.out.println(byteVar);

	System.out.println("Please enter short");
	shortVar = inputScanner.nextShort();
        System.out.println(shortVar);

	System.out.println("Please enter float");
	floatVar = inputScanner.nextFloat();
        System.out.println(floatVar);

	System.out.println("Please enter double");
	doubleVar = inputScanner.nextDouble();
        System.out.println(doubleVar);

	System.out.println("Please enter boolean");
	booleanVar = inputScanner.nextBoolean();
        System.out.println(booleanVar);
    }
}

